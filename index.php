<?php
require_once 'vendor/autoload.php';
use Symfony\Component\HttpFoundation\Request;
$request = Request::createFromGlobals();
$title = $request->query->get('t','home');
$titles = ['home','o firmie', 'oferta', 'cennik', 'kontakt'];
?>

<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/style.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <title>Biuro rachunkowe</title>
    <link rel="stylesheet" href="css/responsiveslides.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="scripts/responsiveslides.min.js"></script>
    <script>
        // You can also use "$(window).load(function() {"
        $(function () {

            // Slideshow 2
            $("#slider2").responsiveSlides({
                auto: true,
                pager: false,
                speed: 400,
                maxwidth: 932
            });
            // Slideshow 4
            $("#slider4").responsiveSlides({
                auto: false,
                pager: false,
                nav: true,
                speed: 500,
                namespace: "callbacks",
                before: function () {
                    $('.events').append("<li>before event fired.</li>");
                },
                after: function () {
                    $('.events').append("<li>after event fired.</li>");
                }
            });

        });
    </script>
</head>
<body>
    <div class="container-fluid">
        <div class="row top">
            <div class="col-md-5 logo"></div>
            <div class="col-md-2 phone">607 301 018</div>
        </div> <!-- TOP -->
        <div id="content">
            <div class="row">
                <div class="col-md-12 menu">
                    <ul>
                        <li><a href="index.php">Strona Główna</a></li>
                        <li><a href="/o firmie">O firmie</a></li>
                        <li><a href="/oferta">Oferta</a></li>
                        <li><a href="/cennik">Cennik</a></li>
                        <li><a href="/kontakt">Kontakt</a></li>
                    </ul>
                </div>
            </div> <!-- MENU -->
            <div class="row">
                <?php if($title == 'home'): ?>
                <div class="col-md-12 big-slider">
                    <ul class="rslides" id="slider2">
                        <li><a href="?t=kontakt"><img src="img/slider/home.jpg" alt=""></a></li>
                        <li><a href="?t=cennik"><img src="img/slider/cennik.jpg" alt=""></a></li>
                        <li><a href="?t=o firmie"><img src="img/slider/o%20firmie.jpg" alt=""></a></li>
                        <li><a href="?t=oferta"><img src="img/slider/oferta.jpg" alt=""></a></li>
                    </ul>
                </div>
                <?php else: ?>
                <div class="col-md-12 slider">
                    <img src="slider/<?php echo $title; ?>.jpg">
                </div>
                <?php endif; ?>
            </div> <!-- SLIDER -->
            <?php
            try {
                if(in_array($title,$titles)) {
                    $file = new SplFileInfo("sites/$title.html");
                } else {
                    echo 'Brak dostępu do strony!';
                }
            } catch (Exception $e) {
                echo $e->getMessage();
            }
            ?>
            <div class="row">
                <?php if($title != 'home'):?>
                <div class="col-md-12 content topic">
                    Strona Główna >> <?php echo ucfirst($title)?>
                    <hr class="hr">
                </div>
                <?php endif; ?>
            </div> <!-- TOPIC -->
            <div class="row">
                <div class="col-md-12 content">
                    <?php if($title != 'kontakt' && $title != 'home'):?>
                    <div class="subject"><?php echo ucfirst($title)?></div>
                    <?php endif;?>
                    <div class="content-container">
                        <?php
                        foreach ($file->openFile('r') as $line) {
                            if(strlen($line) == 1){
                                echo '<br><br>';
                            } else {
                                echo $line;
                            }
                        }
                        ?>
                    </div>
                </div>
            </div> <!-- CONTENT -->
        </div>
        <div class="row bottom">
            <div class="row bottom-content">
                <div class="col-md-6">
                    <ul>
                        <h3>USŁUGI</h3>
                        <li>Obsługa księgowa</li>
                        <li>Obsługa kadrowo-płacowa</li>
                        <li>Odbiór dokumentów od Klienta</li>
                        <li>Przygotowanie przelewów bankowych</li>
                        <li>Przypomnienia o płatnościach podatników</li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <h3>KONTAKT</h3>
                    Biuro Rachunkowe Lucyna Przybył<br>
                    ul. Henryka Sienkiewicza 5<br>
                    63-800 Gostyń, Polska<br>
                    tel. +48 607 301 018<br>
                    e-mail: lprzybyl@op.pl
                </div>
            </div>

            <div class="row footer">
                <div class="col-md-6">
                    Lucyna Przybył Sp. C. &copy; Copyright 2014-2015
                </div>
                <div class="col-md-6">
                    <a href="index.php">Strona główna</a> / <a href="?t=o firmie">O firmie</a> / <a href="?t=oferta">Oferta</a> / <a href="?t=cennik">Cennik</a> / <a href="?t=kontakt">Kontakt</a>
                </div>
            </div>
        </div> <!-- BOTTOM -->
    </div>
</body>
</html>
